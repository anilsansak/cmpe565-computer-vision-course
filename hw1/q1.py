
import cv2
import numpy as np
from skimage import color

lena = cv2.imread("../lena.png", 0)
image = color.rgb2gray(lena)


sobelx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], dtype=np.float)
sobely = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]], dtype=np.float)

row = image.shape[0]
column = image.shape[1]

sobelxImage = np.zeros((row, column))
sobelyImage = np.zeros((row, column))
sobelGrad = np.zeros((row, column))

image = np.pad(image, (1, 1), 'edge')

for i in range(1, row-1):
    for j in range(1, column-1):
        gx = (sobelx[0][0] * image[i-1][j-1]) + (sobelx[0][1] * image[i-1][j]) + \
             (sobelx[0][2] * image[i-1][j+1]) + (sobelx[1][0] * image[i][j-1]) + \
             (sobelx[1][1] * image[i][j]) + (sobelx[1][2] * image[i][j+1]) + \
             (sobelx[2][0] * image[i+1][j-1]) + (sobelx[2][1] * image[i+1][j]) + \
             (sobelx[2][2] * image[i+1][j+1])

        gy = (sobely[0][0] * image[i-1][j-1]) + (sobely[0][1] * image[i-1][j]) + \
             (sobely[0][2] * image[i-1][j+1]) + (sobely[1][0] * image[i][j-1]) + \
             (sobely[1][1] * image[i][j]) + (sobely[1][2] * image[i][j+1]) + \
             (sobely[2][0] * image[i+1][j-1]) + (sobely[2][1] * image[i+1][j]) + \
             (sobely[2][2] * image[i+1][j+1])

        sobelxImage[i-1][j-1] = gx
        sobelyImage[i-1][j-1] = gy

        g = np.sqrt(gx * gx + gy * gy)
        sobelGrad[i-1][j-1] = g

cv2.imwrite('gx.png', sobelxImage)
cv2.imwrite('gy.png', sobelyImage)
cv2.imwrite('gradient.png', sobelGrad)
