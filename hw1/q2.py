import cv2
import numpy as np
from skimage import io
from scipy import ndimage

cat_img = io.imread("data/cat.bmp")
dog_img = io.imread("data/dog.bmp")


def makeHybrid(highFreqImg, lowFreqImg, highFreqSigma, lowFreqSigma):
    highPassed = highPass(highFreqImg, highFreqSigma)
    lowPassed = lowPass(lowFreqImg, lowFreqSigma)
    return highPassed + lowPassed


def highPass(image, sigma):
    return image - gaussianFilter(image, sigma)


def lowPass(image, sigma):
    return gaussianFilter(image, sigma)


def gaussianFilter(image, sigma):
    return ndimage.gaussian_filter(image, sigma)


def im2double(im):
    min_val = np.min(im.ravel())
    max_val = np.max(im.ravel())
    out = (im.astype('float') - min_val) / (max_val - min_val)
    return out


cat = im2double(cat_img)
dog = im2double(dog_img)

hybridImage = makeHybrid(cat, dog, 11, 6)
io.imshow(hybridImage)
io.show()
