import cv2
import numpy as np
from skimage import io


def im2double(im):
    min_val = np.min(im.ravel())
    max_val = np.max(im.ravel())
    out = (im.astype('float') - min_val) / (max_val - min_val)
    return out


def imresize(image, scalePercent):
    width = int(image.shape[1] * scalePercent / 100)
    height = int(image.shape[0] * scalePercent / 100)
    dim = (width, height)
    resized = cv2.resize(image, dim, fx=scalePercent/100, fy=scalePercent/100)
    return resized


def gaussPyramid(image, numOfLevels):
    out = []
    sigma = 3
    out.append(image)  # g0
    for i in range(numOfLevels):
        g = cv2.GaussianBlur(out[i], (0, 0), sigma,
                             borderType=cv2.BORDER_DEFAULT)
        g = imresize(g, 50)
        out.append(g)
    return out


def laplPyramid(gaussPyr):
    out = []

    for i in range(len(gaussPyr) - 1):
        if(i < 4):
            l = gaussPyr[i] - imresize(gaussPyr[i+1], 200)
            out.append(l)
        else:
            out.append(gaussPyr[i])
    return out


def collapse(laplPyr):
    out = imresize(laplPyr[4], 200) + laplPyr[3]
    out = imresize(out, 200) + laplPyr[2]
    out = imresize(out, 200) + laplPyr[1]
    out = imresize(out, 200) + laplPyr[0]
    return out


def blend(firstImgLapl, secondImgLapl, gaussMask):
    out = []
    for i in range(len(firstImgLapl)):
        out.append((gaussMask[i] * firstImgLapl[i]) +
                   ((1 - gaussMask[i]) * secondImgLapl[i]))

    return out


face = cv2.imread("blending/face.jpeg", cv2.IMREAD_GRAYSCALE)
hand = cv2.imread("blending/hand.jpeg", cv2.IMREAD_GRAYSCALE)
mask = cv2.imread("blending/mask.jpeg", cv2.IMREAD_GRAYSCALE)

face = cv2.resize(face, (256, 256))
hand = cv2.resize(hand, (256, 256))
mask = cv2.resize(mask, (256, 256))

face = im2double(face)
hand = im2double(hand)
mask = im2double(mask)

numOfLevels = 5

gaussFace = gaussPyramid(face, numOfLevels)
gaussHand = gaussPyramid(hand, numOfLevels)
gaussMask = gaussPyramid(mask, numOfLevels)
laplFace = laplPyramid(gaussFace)
laplHand = laplPyramid(gaussHand)

blended = blend(laplFace, laplHand, gaussMask)
final = collapse(blended)
cv2.imshow("Final", final)
io.imsave("blended.png", final)
cv2.waitKey(0)
cv2.destroyAllWindows()
