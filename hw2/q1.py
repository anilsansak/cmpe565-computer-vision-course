import cv2
import numpy as np
from scipy import signal as sig
from scipy import ndimage as ndi


# Function to calculate the derivative of gradient in x axis


def gradient_x(imggray):
    kernel_x = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
    return sig.convolve2d(imggray, kernel_x, mode='same')

# Function to calculate the derivative of gradient in y axis


def gradient_y(imggray):
    kernel_y = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])
    return sig.convolve2d(imggray, kernel_y, mode='same')

# Function to find corners by Using Harris Corner Detection method


def HarrisCornerDetector(image, sigma, thres):
    cornerPts = 1
    numCornerPts = 0

    I_x = gradient_x(image)
    I_y = gradient_y(image)

    # Add Gaussian blur
    Ixx = ndi.gaussian_filter(I_x**2, sigma)
    Ixy = ndi.gaussian_filter(I_y*I_x, sigma)
    Iyy = ndi.gaussian_filter(I_y**2, sigma)

    height = image.shape[0]
    width = image.shape[1]

    cornerList = []
    newImg = image.copy()
    offset = 0
    k = 0.05
    color_img = cv2.cvtColor(newImg, cv2.COLOR_GRAY2RGB)

    # Loop through image and find our corners
    for y in range(offset, height-offset):
        for x in range(offset, width-offset):
            # Calculate sum of squares
            windowIxx = Ixx[y-offset:y+offset+1, x-offset:x+offset+1]
            windowIxy = Ixy[y-offset:y+offset+1, x-offset:x+offset+1]
            windowIyy = Iyy[y-offset:y+offset+1, x-offset:x+offset+1]
            Sxx = windowIxx.sum()
            Sxy = windowIxy.sum()
            Syy = windowIyy.sum()

            # Find determinant and trace, use to get corner response
            det = (Sxx * Syy) - (Sxy**2)
            trace = Sxx + Syy
            r = det - k*(trace**2)

            # If corner response is over threshold, color the point and add to corner list
            if r > thres:
                cornerList.append([x, y, r])
                color_img.itemset((y, x, 0), 0)
                color_img.itemset((y, x, 1), 0)
                color_img.itemset((y, x, 2), 255)

    return color_img, cornerList


# Read the image and convert it to grayscale
image = cv2.imread("data/Boxes.png", cv2.IMREAD_GRAYSCALE)

# Conver the image to 32 bit floating point
image = np.float32(image)
newImg, cornerList = HarrisCornerDetector(image, 1, 1)


# Show the image
# cv2.imshow("Original Image", image)
# cv2.imshow("Corners", newImg)
cv2.imwrite("1a.png", newImg)
if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()
