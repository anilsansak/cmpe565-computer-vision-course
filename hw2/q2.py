import cv2
import numpy as np

image1 = cv2.imread("data/Rainier1.png", cv2.IMREAD_GRAYSCALE)
image2 = cv2.imread("data/Rainier2.png", cv2.IMREAD_GRAYSCALE)

sift = cv2.xfeatures2d_SIFT().create()
kp1, des1 = sift.detectAndCompute(image1, None)
kp2, des2 = sift.detectAndCompute(image2, None)
bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=False)
matches = bf.match(des1, des2)
matches = sorted(matches, key=lambda x: x.distance)
matching_result = cv2.drawMatches(
    image1, kp1, image2, kp2, matches[:50], None, flags=2)

img1 = cv2.drawKeypoints(image1, kp1, None)
img2 = cv2.drawKeypoints(image2, kp2, None)
cv2.imshow("Image 1", img1)
cv2.imshow("Image 2", img2)
cv2.imshow("Image", matching_result)

cv2.imwrite("2a.png", img1)
cv2.imwrite("2b.png", img2)
cv2.imwrite("2c.png", matching_result)

cv2.waitKey(0)
cv2.destroyAllWindows()
